﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPFBankApp2
{
    internal class Manager : Consultant
    {
        public static void ChangeName(int id, string NewName, string OldName)
        {
            foreach (ClientData client in clients)
            {
                if (client.Index == id)
                {
                    string oldFirstName = OldName;
                    string newName = NewName;
                    client.FirstName = newName;
                    client.FirstNameChanged = $"Имя клиента: {oldFirstName} изменено на: {newName}\nДата изменения: {DateTime.Now} \nИзменено: Менеджер";
                    SerializeClient(clients, path);
                }
            }
        }

        public static void ChangeLastName(int id, string NewLastName, string OldLastName)
        {
            foreach (ClientData client in clients)
            {
                if (client.Index == id)
                {
                    string oldLastName = OldLastName;
                    string newLastName = NewLastName;
                    client.LastName = newLastName;
                    client.LastNameChanged = $"Имя клиента: {oldLastName} изменено на: {newLastName}\nДата изменения: {DateTime.Now} \nИзменено: Менеджер";
                    SerializeClient(clients, path);
                }
            }
        }

        public static void ChangePasport(int id, int NewPasport, int OldPasport)
        {
            foreach (ClientData client in clients)
            {
                if (client.Index == id)
                {
                    int newPasport = NewPasport;
                    if (ClientData.PasportNum.Contains(newPasport))
                    {
                        MessageBox.Show("Клинет с таким паспортом уже зарегистрирован!");
                    }
                    else
                    {
                        int oldPasporNumb = OldPasport;
                        ClientData.PasportNum.Remove(client.Pasport);
                        client.Pasport = newPasport;
                        client.PasportChanged = $"Номер паспорта клиента был изменен! \nСтарый номер: {oldPasporNumb} новый номер: {newPasport}\nДата изменения: {DateTime.Now} \nИзменено: Менеджер ";
                        SerializeClient(clients, path);
                        ClientData.PasportNum.Add(newPasport);
                    }
                }
            }
        }

        public static void ChangePhone(int id, int NewPhone, int OldPhone)
        {
            int newPhone = NewPhone;

            if (ClientData.PhoneNum.Contains(newPhone))
            {
                MessageBox.Show("Клинет с таким номером телефона уже зарегистрирован!");
            }
            else
            {
                foreach (ClientData client in clients)
                {
                    if (client.Index == id)
                    {
                        int oldPhone = OldPhone;
                        ClientData.PhoneNum.Remove(client.Phone);
                        client.Phone = newPhone;
                        client.PhoneChanged = $"Номер клиента был изменен! \nСтарый номер: {oldPhone} новый номер: {newPhone}\nДата изменения: {DateTime.Now} \nИзменено: Менеджер";
                        SerializeClient(clients, path);
                        ClientData.PhoneNum.Add(newPhone);
                    }
                }
            }
        }

        public void AddClient(string Name, string LastName, int Phone, int Pasport)
        {
            int index = clients.Count;
            string name = Name;
            string lastName = LastName;
            int number = Phone;
            int pasport = Pasport;
            clients.Add(new ClientData(index, name, lastName, number, pasport));
            SerializeClient(clients, path);
        }

        public void SaveChanges(int id)
        {
            foreach (ClientData client in clients)
            {
                if (client.Index == id)
                {
                    client.SaveChanges();
                    SerializeClient(clients, path);
                }
            }
        }
    }
}
