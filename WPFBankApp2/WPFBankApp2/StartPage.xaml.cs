﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFBankApp2
{
    /// <summary>
    /// Логика взаимодействия для StartPage.xaml
    /// </summary>
    public partial class StartPage : Page
    {
        static MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;

        
        static Frame mainFrame = mainWindow.FindName("MainFrame") as Frame;

        public StartPage()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (WorkerStatus.SelectedItem == ConsultCombo)
            {
                mainFrame.Content = MainWindow.pageConsultant;
                MainWindow.pageConsultant.RefreshData();
            }

            if (WorkerStatus.SelectedItem == ManagerCombo)
            {
                mainFrame.Content = MainWindow.pageManager;
                MainWindow.pageManager.RefreshData();
            }
        }
    }
}
