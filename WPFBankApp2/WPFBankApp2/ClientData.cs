﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFBankApp2
{
    public class ClientData
    {
        static Random rnd;
        public static List<int> PhoneNum;
        public static List<int> PasportNum;
        public string FirstNameChanged = string.Empty;
        public string LastNameChanged = string.Empty;
        public string PhoneChanged = string.Empty;
        public string PasportChanged = string.Empty;

        static ClientData()
        {
            rnd = new Random();
            PhoneNum = new List<int>();
            PasportNum = new List<int>();
        }
        public int Index { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Phone { get; set; }

        public int Pasport { get; set; }

        public ClientData(int index, string firstName, string lastName, int phone, int pasport)
        {
            if (ClientData.PhoneNum.Contains(phone))
            {
                phone = rnd.Next(100000, 999999);
            }
            ClientData.PhoneNum.Add(phone);
            if (ClientData.PasportNum.Contains(pasport))
            {
                pasport = rnd.Next(100000, 999999);
            }
            this.Index = index;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Phone = phone;
            this.Pasport = pasport;
        }

        public ClientData() : this(2, "", "", 1, 0)
        { }
        public void SaveChanges()
        {
            FirstNameChanged = string.Empty;
            LastNameChanged = string.Empty;
            PhoneChanged = string.Empty;
            PasportChanged = string.Empty;
        }
        public class SortByName : IComparer<ClientData>
        {
            public int Compare(ClientData x, ClientData y)
            {
                ClientData X = (ClientData)x;
                ClientData Y = (ClientData)y;

                return String.Compare(X.FirstName, Y.FirstName);
            }
        }

        public class SortByLast : IComparer<ClientData>
        {
            public int Compare(ClientData x, ClientData y)
            {
                ClientData X = (ClientData)x;
                ClientData Y = (ClientData)y;

                return String.Compare(X.LastName, Y.LastName);
            }
        }
    }
}

