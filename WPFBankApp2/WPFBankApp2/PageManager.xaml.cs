﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFBankApp2
{
    /// <summary>
    /// Логика взаимодействия для PageManager.xaml
    /// </summary>
    public partial class PageManager : Page
    {
        static MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
        static Frame mainFrame = mainWindow.FindName("MainFrame") as Frame;
        static Manager manager = new Manager();

        private string originalText;
        private string tempName;
        private string tempLastName;
        private int tempPhone;
        private int tempPasport;

        public PageManager()
        {
            InitializeComponent();
            Consultant.LoadClients();
            DataBankClientManager.ItemsSource = Consultant.clients;
            AddStack.Visibility = Visibility.Collapsed;
            SaveAddeedClient.Visibility = Visibility.Collapsed;
            CancelAddeedClient.Visibility = Visibility.Collapsed;
            DataBankClientManager.AddHandler(GridViewColumnHeader.ClickEvent, new RoutedEventHandler(OnHeaderClick));
        }

        public void RefreshData()
        {
            Consultant.LoadClients();
            DataBankClientManager.ItemsSource = Consultant.clients;
            DataBankClientManager.Items.Refresh();
        }
        
        private void OnHeaderClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader columnHeader = e.OriginalSource as GridViewColumnHeader;

            if (columnHeader != null)
            {
                string columnText = columnHeader.Content as string;

                if (!string.IsNullOrEmpty(columnText))
                {
                    if (columnText == "Имя")
                    {
                        Consultant.SortByFirstName();
                    }
                    else if (columnText == "Фамилия")
                    {
                        Consultant.SortByLastName();
                    }

                    DataBankClientManager.Items.Refresh();
                }
            }
        }

        private void AddClient_Click(object sender, RoutedEventArgs e)
        {
            AddClientName.Text = string.Empty;
            AddClientLastName.Text = string.Empty;
            AddClientPhone.Text = string.Empty;
            AddClientPasport.Text = string.Empty;
            ButtonsStack.Visibility = Visibility.Collapsed;
            CancelAddeedClient.Visibility= Visibility.Visible;
            SaveAddeedClient.Visibility = Visibility.Visible;
            AddStack.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Content = MainWindow.startPage;
            DataBankClientManager.Items.Refresh();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Consultant.FillRandomClients();
            DataBankClientManager.Items.Refresh();
        }

        private void ChangeNameButton_Click(object sender, RoutedEventArgs e)
        {
            int Index = int.Parse(IDTextBlock.Text);
            originalText = MClientName.Text;
            Manager.ChangeName(Index, originalText, tempName);
        }

        private void ChangeLastName_Click(object sender, RoutedEventArgs e)
        {
            int Index = int.Parse(IDTextBlock.Text);
            originalText = MClientLastName.Text;
            Manager.ChangeLastName(Index, originalText, tempLastName);
        }

        private void ChangePhoneManager_Click(object sender, RoutedEventArgs e)
        {
            int Index = int.Parse(IDTextBlock.Text);
            int NewPhone = int.Parse(MClientPhone.Text);
            Manager.ChangePhone(Index, NewPhone, tempPhone);
        }

        private void ChangePasport_Click(object sender, RoutedEventArgs e)
        {
            int Index = int.Parse(IDTextBlock.Text);
            int NewPaport = int.Parse(MClientPasport.Text);
            Manager.ChangePasport(Index, NewPaport, tempPasport);
        }


        private void SaveAddeedClient_Click(object sender, RoutedEventArgs e)
        {
            if (AddClientName.Text == string.Empty || AddClientLastName.Text == string.Empty || AddClientPhone.Text == string.Empty || AddClientPasport.Text == string.Empty)
            {
                MessageBox.Show("Введите данные клиента!");
            }
            else
            {
                string name = AddClientName.Text;
                string lastName = AddClientLastName.Text;
                int number = int.Parse(AddClientPhone.Text);
                int pasport = int.Parse(AddClientPasport.Text);
                manager.AddClient(name, lastName, number, pasport);
                SaveAddeedClient.Visibility = Visibility.Collapsed;
                CancelAddeedClient.Visibility = Visibility.Collapsed;
                AddStack.Visibility = Visibility.Collapsed;
                ButtonsStack.Visibility = Visibility.Visible;

                DataBankClientManager.ItemsSource = Consultant.clients;
                DataBankClientManager.Items.Refresh();
            }
        }

        private void DataBankClientManager_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IDTextBlock.Text != string.Empty)
            {
                int Index = int.Parse(IDTextBlock.Text);
                foreach (ClientData client in Consultant.clients)
                {
                    if (client.Index == Index)
                    {
                        FirstNameChangedText.Text = client.FirstNameChanged;
                        LastNameChangedText.Text = client.LastNameChanged;
                        PhoneChangedText.Text = client.PhoneChanged;
                        PasportNameChangedText.Text = client.PasportChanged;

                        tempLastName = client.LastName;
                        tempName = client.FirstName;
                        tempPhone = client.Phone;
                        tempPasport = client.Pasport;
                    }
                }
            }
        }

        private void CancelAddeedClient_Click(object sender, RoutedEventArgs e)
        {
            SaveAddeedClient.Visibility = Visibility.Collapsed;
            AddStack.Visibility = Visibility.Collapsed;
            ButtonsStack.Visibility = Visibility.Visible;
            CancelAddeedClient.Visibility = Visibility.Collapsed;
            AddClientName.Text = string.Empty;
            AddClientLastName.Text = string.Empty;
            AddClientPhone.Text = string.Empty;
            AddClientPasport.Text = string.Empty;
        }
    }
}
