﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;

namespace WPFBankApp2
{
    internal class Consultant
    {
        public static List<ClientData> clients = new List<ClientData>();
        public static string path = "BancClientsData.txt";
        public static string toPrint = string.Empty;

        public static void FillRandomClients()
        {
            Random random = new Random();
            if (File.Exists(path))
            {
                int MaxIndex = 0;
                MaxIndex = clients.Count;
                for (int i = MaxIndex; i <= MaxIndex + 9; i++)
                {
                    clients.Add(new ClientData(i, $"Name_{i}", $"LastName_{i}", random.Next(100000, 999999), random.Next(100000, 999999)));
                }
            }
            else
            {
                for (int i = 0; i <= 10; i++)
                {
                    clients.Add(new ClientData(i, $"Name_{i}", $"LastName_{i}", random.Next(100000, 999999), random.Next(100000, 999999)));
                }
            }
            SerializeClient(clients, path);
        }

        public static void ChangePhone(int id, int NewPhone, int OldPhone)
        {
            int newPhone = NewPhone;

            if (ClientData.PhoneNum.Contains(newPhone))
            {
                MessageBox.Show("Клинет с таким номером телефона уже зарегистрирован!");
            }
            else
            {
                foreach (ClientData client in clients)
                {
                    if (client.Index == id)
                    {
                        int oldPhone = OldPhone;
                        ClientData.PhoneNum.Remove(client.Phone);
                        client.Phone = newPhone;
                        client.PhoneChanged = $"Номер клиента был изменен! \nСтарый номер: {oldPhone} новый номер: {newPhone}\nДата изменения: {DateTime.Now} \nИзменено: Консультант";
                        SerializeClient(clients, path);
                        ClientData.PhoneNum.Add(newPhone);
                    }
                }
            }
        }
        static public void SerializeClient(List<ClientData> client, string Path)
        {
            using (StreamWriter writer = new StreamWriter(Path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<ClientData>));
                serializer.Serialize(writer, client);
            }
        }

        static public List<ClientData> DesiriaalizeClients(string Path)
        {
            if (File.Exists(path))
            {
                List<ClientData> tempClients = new List<ClientData>();
                XmlSerializer serializer = new XmlSerializer(typeof(List<ClientData>));
                Stream stream = new FileStream(Path, FileMode.Open, FileAccess.Read);
                tempClients = serializer.Deserialize(stream) as List<ClientData>;
                stream.Close();
                return tempClients;
            }
            else return null;
        }

        static public void SerializeChanges(ClientData client, string Path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ClientData));
            Stream stream = new FileStream(Path, FileMode.Create, FileAccess.Write);
            serializer.Serialize(stream, client);
            stream.Close();
        }

        public static void LoadClients()
        {
            if (File.Exists(path))
                clients = DesiriaalizeClients(path);
            else MessageBox.Show("Файл не найден");
        }

        public static void SortByFirstName()
        {
            clients.Sort(new ClientData.SortByName());
        }
        public static void SortByLastName()
        {
            clients.Sort(new ClientData.SortByLast());
        }
    }
}
