﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFBankApp2
{
    /// <summary>
    /// Логика взаимодействия для PageConsultant.xaml
    /// </summary>
    public partial class PageConsultant : Page
    {
        static MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
        static Frame mainFrame = mainWindow.FindName("MainFrame") as Frame;
        private int tempPhone;
        //static Consultant consultant = new Consultant();
        public PageConsultant()
        {
            InitializeComponent();
            Consultant.LoadClients();
            DataBankClient.ItemsSource = Consultant.clients;
            DataBankClient.AddHandler(GridViewColumnHeader.ClickEvent, new RoutedEventHandler(OnHeaderClick));
        }

        private void OnHeaderClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader columnHeader = e.OriginalSource as GridViewColumnHeader;

            if (columnHeader != null)
            {
                string columnText = columnHeader.Content as string;

                if (!string.IsNullOrEmpty(columnText))
                {
                    if (columnText == "Имя")
                    {
                        Consultant.SortByFirstName();
                    }
                    else if (columnText == "Фамилия")
                    {
                        Consultant.SortByLastName();
                    }

                    DataBankClient.Items.Refresh();
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Content = MainWindow.startPage;
        }

        private void ChangePhone_Click(object sender, RoutedEventArgs e)
        {
            int Index = int.Parse(IDTextBlock.Text);
            int NewPhone = int.Parse(ClientPhone.Text);
            Consultant.ChangePhone(Index, NewPhone, tempPhone);
        }

        public void RefreshData()
        {
            Consultant.LoadClients();
            DataBankClient.ItemsSource = Consultant.clients;
            DataBankClient.Items.Refresh();
        }
    }
}
