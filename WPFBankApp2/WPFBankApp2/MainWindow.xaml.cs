﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFBankApp2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static PageConsultant pageConsultant;
        public static PageManager pageManager;
        public static StartPage startPage;
        public static Frame mainFrame;

        public MainWindow()
        {
            InitializeComponent();
            pageConsultant = new PageConsultant();
            pageManager = new PageManager();
            startPage = new StartPage();
            mainFrame = MainFrame;
            MainFrame.Content = startPage;
        }
    }
}
